import React from 'react';
import './App.css';
import { Login } from './Authentification/Login';
import { Home } from './Home/Home';
import { BrowserRouter as Router, Route } from 'react-router-dom'

function App() {
  return (
    <div className="App">
     <Router>
        <Route path={"/"} component={Login} exact />
        <Route path={"/Home/:code"} component={Home} />
      </Router>
    </div>
  );
}

export default App;
