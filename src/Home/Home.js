import React from 'react';
import { Header } from '../UI/Header';
import '../Home/Home.css'
import { Footer } from '../UI/Footer';

export class Home extends React.Component{

    constructor(){
        super();
        this.state={
            base64: "data:image/jpeg;base64,",
            event: Object,
            ok: false,
            user: Object,
            final_url:[]
        }
    }

    async componentDidMount() {
        const url1 = "https://bip-module.herokuapp.com/api/evenement/";
        const response = await fetch(url1);
        const data_event = await response.json();

        console.log(this.props.location.pathname.split('/'))
        const url2 = "https://bip-module.herokuapp.com/api/client/" + this.props.location.pathname.split('/')[2];
        const response_user = await fetch(url2);
        const data_user = await response_user.json();

        this.setState({
            event: data_event,
            ok: true,
            user: data_user
        });
    }

    render() {
        if (this.state.ok === false)
            return (
            <div>    
                <h4>Veuillez Patienter...</h4>
                <Footer/>
            </div>);
        else {
            console.log(this.state.event)
            return (
                <div>
                    <Header user={this.state.user} />
                    <div className="col-12">
                        <h4>Retrouvez ci dessous tous les évènements près de chez vous</h4>
                    </div>
                    <div className="row col-12">
                        {this.state.event.map(e => (
                            <div className="col-md-3" key={e.id}>
                                <div className="card-wrapper">
                                    <div id="card-1" className="card-rotating effect__click text-center h-100 w-100 z-depth-1">

                                        <div className="face front card taille">
                                            <div className="card-up card-up-bb">
                                                <img className="card-img-top image_size" src={this.state.base64 + e.img} alt="image" />
                                            </div>
                                            <div className="font-weight-bold white-custom vol-location text-left">


                                            </div>
                                            <div className="avatar mx-auto white">
                                                <i className="fa fa-music rounded-circle icon-result"></i>
                                            </div>

                                            <div className="card-body card-body-content">
                                                <h6 className="font-weight-bold"></h6>
                                                <div className="container-fluid">
                                                    <div className="row">
                                                        <span className="font-weight-bold col-md-12 font-size-15  blue-text text-left">{e.designation}</span>
                                                        <p className="col-md-12  font-size-14 blue-text">A partir de {e.tarifications[0].prix} FCFA</p>
                                                        <p className="col-md-12 font-size-14 blue-text">  le {e.dateEvenement} à  {e.heureDebut}</p>
                                                    </div>
                                                </div>
                                                <br />
                                                <button className="btn btn-success float-right">Acheter</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                <Footer/>
                </div>
            );
        }
    }
}