import React from 'react';
import logo from '../logo.png';

export class Header extends React.Component {
  
        state = {
            user: Object
        }
 
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#"> <img src={logo} alt="logo" height="40" width="50" /></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="#">Accueil<span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Nous contacter</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">A propos</a>
                        </li>
                    </ul>
                    <span className="navbar-text">
                       {this.props.user.nom +' '+ this.props.user.prenom} 
                    </span>
                </div>
            </nav>)
    }
}