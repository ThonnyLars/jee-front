import React from 'react';
import axios from 'axios';

export class Login extends React.Component {

    constructor() {
        super();
        this.state = {
            userEmail: '',
            userpwd: '',
            user: Object
        }
    }
    getEmail(event) {
        this.setState({
            userEmail: event.target.value
        });
    }
    getPwd(event) {
        this.setState({
            userpwd: event.target.value
        });
    }

    Connexion(event) {
        event.preventDefault();
        axios.post("https://bip-module.herokuapp.com/api/client/{email}/{pw}?email=" + this.state.userEmail + "&pw=" + this.state.userpwd)
            .then(response => {
                this.setState({
                    user: response.data
                });     
                this.props.history.push("/Home/"+this.state.user.code);
            });
       
    }

    render() {
        return (
            <div>
                <div className="container mt-5">
                    <div className="row ">

                        <div className="card mx-auto">
                            <div className="card-body">
                                <form className="text-center  p-5">
                                    <div className="form-group">
                                        <label>Entrer votre Email</label>
                                        <input
                                            value={this.state.userEmail}
                                            autoComplete="off"
                                            type="email"
                                            className="form-control"
                                            onChange={this.getEmail.bind(this)}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Entrer votre mot de passe</label>
                                        <input
                                            value={this.state.userpwd}
                                            autoComplete="off"
                                            type="password"
                                            className="form-control"
                                            onChange={this.getPwd.bind(this)}
                                        />
                                    </div>

                                    <button type="submit" onClick={this.Connexion.bind(this)} className="btn btn-primary">Valider</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}